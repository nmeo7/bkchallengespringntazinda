package com.example.techouse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.data.domain.Sort;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.*;

@RestController
public class MainController {
  @Autowired
  private ProductRepository productRepository;

  private class ErrorMessage {
    public Integer status;
    public String error;
    public String message;

    public ErrorMessage()
    {
      status = 200;
      error = "Success!";
      message = "";
    }
  }

  @PostMapping(path="/add")
  public ErrorMessage addNewProduct (@RequestBody Product product) {

    ErrorMessage e = new ErrorMessage();
    
    if (productRepository.save(product) == null)
    {
      e.status = 500;
      e.error = "Cannot save product.";
    }
    
    return e;
  }

  @GetMapping(path="/all")
  public List<Product> getAllProducts() {
    return productRepository.findTop10ByOrderByNameAsc (  );
  }
}