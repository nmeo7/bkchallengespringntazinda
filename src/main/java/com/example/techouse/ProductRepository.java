package com.example.techouse;

import java.util.Collection; 
import java.util.*;

import org.springframework.data.repository.CrudRepository;

import com.example.techouse.Product;
import org.springframework.data.jpa.repository.Query;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface ProductRepository extends CrudRepository<Product, Integer> {

	public List<Product> findTop10ByOrderByNameAsc();

}